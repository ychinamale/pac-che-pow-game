#! /usr/bin/env python
"""
Assessment:			AI Final Project
Project Title:		Pac-Che-Pow Game
Author: 			Yamikani Chinamale
Institution:		University of the Witwatersrand
Department:			Computer Science and Applied Mathematics
"""

import string, sys
from random import randint

class PCPBoard:

	# Player 1 is 1
	# Player 2 is 2

	# playerlocation = [ [], [] ]
	playerlocation = []

	def __init__(self):
		self.boxes = [\
			['_','o','_','o','_','o','_'],\
			['o','_','o','_','o','_','o'],\
			['_','o','_','o','_','o','_'],\
			['o','_','A','o','P','_','o'],\
			['_','o','_','o','_','o','_'],\
			['o','_','o','_','o','_','o'],\
			['_','o','_','o','_','o','_'],\
		]
		# No moves have yet been made
		self.started = False

		# track each player's coordinates
		#self.playerlocation[0] = [3,2] # Human
		#self.playerlocation[1] = [3,4] # AI

		for r in range(7):
			for c in range(7):
				if(self.boxes[r][c] == 'P'):
					#self.playerlocation[0] = [r,c]
					#self.playerlocation.append((r,c))
					humanlocation = (r,c)
				if(self.boxes[r][c] == 'A'):
					#self.playerlocation.append((r,c))
					#self.playerlocation[1] = [r,c]
					AIlocation = (r,c)

		self.playerlocation.append(humanlocation)
		self.playerlocation.append(AIlocation)
		
		# set move type for each player
		# movetype[0] for Human
		# movetype[1] for AI
		# value 1 => diagonal
		# value 2 => perpendicular
		#self.movetype = [1,1]

		# keep the player scores
		self.playerscore = [0,0]

		# keep the player moves
		self.playermoves = [0,0]

	def GetState(self):
		return self.boxes

	def IsStart(self):
		return self.started

	def GetTurn(self):
		return self.playertomove

	def SetStarted(self):
		self.started = True

	def SetPlayerLocation(self, playerindex, location):
		self.playerlocation[playerindex][0] = location[0];
		self.playerlocation[playerindex][1] = location[1];

	def MovePlayer(self, movetuple, playerindex):
		
		from_row = movetuple[0][0]
		from_col = movetuple[0][1]
		to_row = movetuple[1][0]
		to_col = movetuple[1][1]

		# get the value of the player moving
		player = self.boxes[from_row][from_col]

		# get the value of the box being moved to
		topiece = self.boxes[to_row][to_col]

		if(topiece == 'o'):
			self.playerscore[playerindex] += 1

		# update current player location to empty
		self.boxes[from_row][from_col] = '_'

		# update destination box as having player
		self.boxes[to_row][to_col] = player

		self.playermoves[playerindex] += 1
		

	def GetMoveCoordinates(self, playerposition, move):
		'''
		Gets a number as move
		Returns the destination as (row, col) tuple
		'''
		col = 0
		row = 0

		if(move==8): # up
			row = playerposition[0]-1
			col = playerposition[1]
		if(move==2): # down
			row = playerposition[0]+1
			col = playerposition[1]
		if(move==4): # left
			row = playerposition[0]
			col = playerposition[1]-1
		if(move==6): # right
			row = playerposition[0]
			col = playerposition[1]+1
		
		return (row, col)

	def PowPlayer(self, tolocation, playerindex):

		'''

		'''
		if(playerindex==0):
			player = "P"
		else:
			player = "A"

		row = tolocation[0]
		col = tolocation[1]
		self.boxes[row][col] = "_"

		# send the player to some random corner
		corner = randint(0,3)

		if(corner==0):
			self.boxes[0][0] = player
			self.playerlocation[playerindex] = (0,0)
		elif(corner==1):
			self.boxes[0][6] = player
			self.playerlocation[playerindex] = (0,6)
		elif(corner==2):
			self.boxes[6][0] = player
			self.playerlocation[playerindex] = (6,0)
		else:
			self.boxes[6][6] = player
			self.playerlocation[playerindex] = (6,6)


	def DeductPoint(self, playerindex):

		self.playerscore[playerindex] -= 1

	def RestorePellet(self, tolocation):

		row = tolocation[0]
		col = tolocation[1]
		self.boxes[row][col] = "o"