#! /usr/bin/env python
"""
Assessment:			AI Final Project
Project Title:		Pac-Che-Pow Game
Author: 			Yamikani Chinamale
Institution:		University of the Witwatersrand
Department:			Computer Science and Applied Mathematics
"""

class PCPRules:

	def GetLegalMoveActions(self, board, playerindex):
		'''
		Returns a list of tuples denoting move actions
		(-1,0) -> up | (1,0) -> down | etc
		'''
		playerlocation = board.playerlocation[playerindex]

		row = playerlocation[0]
		col = playerlocation[1]

		moveactionlist = []

		# if player can move up
		if(row-1 >= 0):
			moveactionlist.append((-1,0))

		# if player can move down
		if(row+1 <= 6):
			moveactionlist.append((1,0))

		# if player can move left
		if(col-1 >= 0):
			moveactionlist.append((0,-1))

		# if player can move right
		if(col+1 <= 6):
			moveactionlist.append((0,1))

		#print ("Entered PCPRules.GetLegalMoveActions()")
		return moveactionlist

	def GetValidMoves(self, board, playerindex, moveactionlist):
		'''
		Returns a list of valid destination tuples
		'''
		opponentindex = (playerindex+1)%2
		opponentlocation = board.playerlocation[opponentindex]
		playerlocation = board.playerlocation[playerindex]

		validdestinations = []

		for action in moveactionlist:
			if((playerlocation[0] + action[0], playerlocation[1] + action[1]) != opponentlocation ):
				validdestinations.append((playerlocation[0] + action[0], playerlocation[1] + action[1]))
		
		#print ("Entered PCPRules.GetValidMoves()")
		return validdestinations

	def IsGameOver(self, board):
		
		pelletsavailable = []
		
		for row in range(7):
			for col in range(7):
				if 'o' in board[row][col]:
					pelletsavailable.append((row,col))

		if len(pelletsavailable) == 0:
			return True
		else:
			return False

	def IsMoveLegal(self, playerlocation, opponentlocation, newlocation):
		
		if(newlocation == opponentlocation):
			print "Illegal move!"
			return False
		if((newlocation[0] < 0) or (newlocation[0] > 6)):
			return False
		if((newlocation[1] < 0) or (newlocation[1] > 6)):
			return False
		return True

	def GetWinner(self, board):
		if(board.playerscore[0] > board.playerscore[1]):
			return 0
		elif(board.playerscore[1] > board.playerscore[0]):
			return 1

	def IsOpponentNear(self, playerlocation, opponentlocation):
		
		oprow = opponentlocation[0]
		opcol = opponentlocation[1]

		row = playerlocation[0]
		col = playerlocation[1]

		minrow = oprow - 1
		maxrow = oprow + 1
		mincol = opcol - 1
		maxcol = opcol + 1
		if(minrow < 0):
			minrow = 0
		if(maxrow > 6):
			maxrow = 6
		if(mincol < 0):
			mincol = 0
		if(maxcol > 6):
			maxcol = 6

		for r in range(minrow, maxrow+1):
			for c in range(mincol, maxcol+1):
				if((r==row) and (c==col)):
					return True

		return False