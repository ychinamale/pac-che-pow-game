#! /usr/bin/env python
"""
Assessment:			AI Final Project
Project Title:		Pac-Che-Pow Game
Author: 			Yamikani Chinamale
Institution:		University of the Witwatersrand
Department:			Computer Science and Applied Mathematics
"""

from PCPRules import PCPRules

class PCPAI:

	utility = []
	for i in range(7):
		utility.append([])

	def __init__(self):
		self.type = 'AI'
		self.Rules = PCPRules()
		self.pelletreward = 12
		self.emptyreward = 0
		self.discount = 0.2

	def GetType(self):
		return self.type
	

class PCPAI_Basic(PCPAI):

	def GetMove(self, board, playerindex):
		'''
		Returns a tuple (fromlocation, tolocation)
		Gets legal moves
		Gets destinations from those moves
		Removes destinations that are in the danger zone
		Gets location with the highest utility from those left
		'''

		opponentindex = (playerindex+1)%2

		actionlist = self.Rules.GetLegalMoveActions(board, playerindex)
		destinationlist = self.Rules.GetValidMoves(board, playerindex, actionlist)

		opponentlocation = board.playerlocation[opponentindex]

		# if a destination in the list is a danger zone, remove it
		iterator = 0
		removeable = []
		while iterator < len(destinationlist):
			if self.Rules.IsOpponentNear(destinationlist[iterator], opponentlocation):
				del destinationlist[iterator]
			else:
				iterator += 1

		# if only one destination is left, return it
		if(len(destinationlist)==1):
			return destinationlist[0]

		else:
			# pick destination with the highest utility
			tolocation = self.GetDestinationMaxUtility(board, destinationlist)
			fromlocation = board.playerlocation[playerindex]

			# return (fromlocation, tolocation) tuple
			return (fromlocation, tolocation)
	
	'''
	def GetUtility(self, board, location, utility, direction):
		
		row = location[0]
		col = location[1]

		if(direction == "up"):
			row = row - 1

		elif(direction == "down"):
			row = row + 1

		elif(direction ==" left"):
			col = col - 1

		elif(direction == "right"):
			col = col + 1

		if(board.boxes[row][col] == "o"):
			return self.pelletreward
		else:
			return utility[row][col]

	'''
	def GetUtility(self, board, directionlist, location, utility):

		utilityupdate = 0
		row = location[0]
		col = location[1]

		for direction in directionlist:
			urow = row
			ucol = col

			if(direction == "up"):
				urow = row - 1

			elif(direction == "down"):
				urow = row + 1

			elif(direction ==" left"):
				ucol = col - 1

			elif(direction == "right"):
				ucol = col + 1

			if(board.boxes[row][col] == "o"):
				utilityupdate += self.pelletreward
			else:
				utilityupdate += self.discount * utility[urow][ucol]

		return utilityupdate


	def GetDestinationMaxUtility(self, board, destinationlist):

		# set rewards for pellets and empty spaces
		self.InitializeUtilities(board)

		# perform value iteration to get utilities of all other spaces
		self.SetAllUtilityValues(board)

		# select destination with highest utility
		destinationtuple = self.GetMaxUtilityTuple(destinationlist)

		return destinationtuple

	def GetMaxUtilityTuple(self, destinationlist):

		row = 0
		col = 0
		maxutility = -500
		maxdestination = -1

		for i in range(len(destinationlist)):
			row = destinationlist[i][0]
			col = destinationlist[i][1]

			thisutility = self.utility[row][col]

			if(thisutility > maxutility):
				maxutility = thisutility
				maxdestination = i

		# return the destination with the max utility
		return destinationlist[maxdestination]


	def InitializeUtilities(self, board):

		for i in range(7):
			for j in range(7):
				if(board.boxes[i][j]=="o"):
					self.utility[i].append(self.pelletreward)
				else:
					self.utility[i].append(self.emptyreward)


	def SetAllUtilityValues(self, board):
		# value iteration
		iterations = 10

		for x in range(iterations):
			for i in range(7):
				for j in range(7):
					
					#if(board.boxes[i][j] != "o"):
					if(i==0): # top row
						if(j==0): # top left corner
							directionlist =["right", "down"]
							self.utility[i][j] = self.GetUtility(board, directionlist, (i,j), self.utility)

						elif(j==6): # top right corner
							directionlist =["left", "down"]
							self.utility[i][j] = self.GetUtility(board, directionlist, (i,j), self.utility)

						else: # middle columns
							directionlist =["left", "right", "down"]
							self.utility[i][j] = self.GetUtility(board, directionlist, (i,j), self.utility)

					elif(i==6): # bottom row
						if(j==0): # bottom left corner
							directionlist =["up", "right"]
							self.utility[i][j] = self.GetUtility(board, directionlist, (i,j), self.utility)

						elif(j==6): # bottom right corner
							directionlist =["up", "left"]
							self.utility[i][j] = self.GetUtility(board, directionlist, (i,j), self.utility)

						else: #middle columns
							directionlist =["up", "left", "right"]
							self.utility[i][j] = self.GetUtility(board, directionlist, (i,j), self.utility)

					else: # middle rows
						if(j==0): # middle rows, first column
							directionlist =["up", "down", "right"]
							self.utility[i][j] = self.GetUtility(board, directionlist, (i,j), self.utility)

						elif(j==6): # middle rows, last column
							directionlist =["up", "down","left"]
							self.utility[i][j] = self.GetUtility(board, directionlist, (i,j), self.utility)

						else: # middle columns
							directionlist =["up", "down", "left", "right"]
							self.utility[i][j] = self.GetUtility(board, directionlist, (i,j), self.utility)