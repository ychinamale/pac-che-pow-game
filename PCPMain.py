#! /usr/bin/env python
"""
Assessment:			AI Final Project
Project Title:		Pac-Che-Pow Game
Author: 			Yamikani Chinamale
Institution:		University of the Witwatersrand
Department:			Computer Science and Applied Mathematics
"""

from PCPBoard import PCPBoard
from PCPRules import PCPRules
from PCPGUI import PCPGUI
from PCPAI import PCPAI_Basic

import sys
import time

class PCPMain:

	def __init__(self):
		# initialize the game board
		self.Board = PCPBoard()

		# set the rules of the game
		self.Rules = PCPRules()

		# set the GUI to be used
		self.GUI = PCPGUI()

		# set the AI to be used
		self.AI = PCPAI_Basic()

	def MainLoop(self):

		# track whose turn it is
		# human starts by default
		# *** CHANGE THIS TO 0 FOR HUMAN TO PLAY FIRST ***
		playerindex = 1

		self.GUI.DrawBoard(self.Board)
		print("AI score: %s, moves %s" % (self.Board.playerscore[1], self.Board.playermoves[1]) )
		print("Human score: %s, moves %s" % (self.Board.playerscore[0], self.Board.playermoves[0]))

		while not self.Rules.IsGameOver(self.Board.GetState()):

			previousscore = self.Board.playerscore[playerindex]

			

			if(playerindex==1):
				print("AI says: It's my turn.")
			else:
				print("AI says: Your turn, human.")

			# identify the opponent
			opponentindex = (playerindex+1)%2

			# get human's move
			if(playerindex == 0):
				# movetuple like (fromlocation, tolocation)
				movetuple = self.GUI.GetUserMove(self.Board, playerindex)

			# get AI's move
			elif(playerindex == 1):
				movetuple = self.AI.GetMove(self.Board, playerindex)

			# change current player's position on the board
			self.Board.MovePlayer(movetuple, playerindex)

			# update the player location using movetuple(fromlocation, tolocation)
			self.Board.playerlocation[playerindex] = movetuple[1]

			# indicate that a move has been made
			self.Board.SetStarted()

			# to find if the opponent is adjacent to player
			powchance = self.Rules.IsOpponentNear(\
				self.Board.playerlocation[playerindex],\
				self.Board.playerlocation[opponentindex]\
				)

			newscore = self.Board.playerscore[playerindex]

			#if(newscore > previousscore):
			#	print("You just ate")

			if not powchance:
				print ("")
			else:
				if(newscore > previousscore):
					self.Board.PowPlayer(movetuple[1], playerindex)
					self.Board.DeductPoint(playerindex)
					self.Board.RestorePellet(movetuple[1])

				else:
					self.Board.PowPlayer(movetuple[1], playerindex)
					
				time.sleep(0.5)
				print("Pow!")
				# check if the previous opponent just ate
				# if yes, restore the pellet
				# cast the opponent to some random corner on the board

			if(playerindex==1):
				time.sleep(0.75)
			# change to the other player's turn
			playerindex = (playerindex+1)%2

			self.GUI.DrawBoard(self.Board)
			print("AI score: %s, moves %s" % (self.Board.playerscore[1], self.Board.playermoves[1]) )
			print("Human score: %s, moves %s" % (self.Board.playerscore[0], self.Board.playermoves[0]))

		print("Game over.")
		if(self.Board.playerscore[0] > self.Board.playerscore[1]):
			print("You win")
		else:
			print("AI wins")


# RUN THE GAME!
print("\n")
ThisGame = PCPMain()
ThisGame.MainLoop()