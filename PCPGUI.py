#! /usr/bin/env python
"""
AI Final Project
Pac-Che-Pow Game

Yamikani Chinamale (1422282)
University of the Witwatersrand
Computer Science and Applied Mathematics
"""

from PCPRules import PCPRules

class PCPGUI:

	def __init__(self):
		self.Rules = PCPRules()

	def DrawBoard(self, board):
		print ""
		for r in range(7):
			for c in range(7):
				if(c==0):
					#sys.stdout.write("|")
					print("|"),
				if(board.boxes[r][c] == "_"):
					#sys.stdout.write("   |")
					print("    |"),
				elif(board.boxes[r][c] == "o"):
					#sys.stdout.write(" o |")
					print(" o  |"),
				elif(board.boxes[r][c] == "A"):
					#sys.stdout.write("o_o|")
					print("[#] |"),
				elif(board.boxes[r][c] == "P"):
					#sys.stdout.write("[*=*]|")
					print("-_- |"),
				else:
					#sys.stdout.write(" "+board.boxes[r][c]+" |")
					print(" "+board.boxes[r][c]+"   |"),
			print "\n"

	def GetUserMove(self, board, playerindex):
		#print "Your turn, human!"
		# get human input
		move = 0

		#tolocation = ()

		while (move == 0):
			'''
			if(board.movetype[playerindex]==1):
				while (move not in ['1','3','7','9']):
					print "\nSelect move: "
					print "\t7   9"
					print "\t  X  "
					print "\t1   3\n"
					
					move = raw_input("=> ")
					print ""
			else:
			'''
			while (move not in ['2','4','6','8']):
				print "\n\tSelect move: "
				print "\t  8  "
				print "\t4 + 6"
				print "\t  2  \n"

				move = raw_input("=> ")
				print ""
			
			# convert user's input to board coordinate
			# get (rol, col) tuple of destination
			tolocation = board.GetMoveCoordinates(\
				board.playerlocation[playerindex], int(move)\
				)

			# used to find the opponent location
			opponentindex = (playerindex+1)%2

			# check if player's attempted move is legal
			if(not self.Rules.IsMoveLegal(\
				board.playerlocation[playerindex],\
				board.playerlocation[opponentindex],\
				tolocation)):
					move = 0 # if move is illegal

			# return tuple of (fromlocation, tolocation)
			else:
			 	movetuple = (\
				board.playerlocation[playerindex],\
				tolocation)

		return movetuple